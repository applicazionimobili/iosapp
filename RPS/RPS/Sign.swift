//
//  Sign.swift
//  RPS
//
//  Created by Nicolas Palermo on 06/03/2020.
//  Copyright © 2020 Nicolas Palermo. All rights reserved.
//

import Foundation
import GameplayKit

/** A randomChoice è assegnato un framework che permette di generare numeri casuali.
    In seguito, nel metodo randomSign, generiamo un numero casuale e a seconda del numero
    generato assegniamo un emoji. */
let randomChoice = GKRandomDistribution(lowestValue: 0, highestValue: 2)

func randomSign() -> Sign {
    let sign = randomChoice.nextInt()
    
    if sign == 0{
        return .rock
    } else if sign == 1{
        return .paper
    } else {
        return .scissors
    }
}

/** Enumerativo che per ogni emoji assegna un valore opportuno.
    Nella funzione turn si calcolano le situazioni di vittoria/pareggio/sconfitta*/
enum Sign{
    case rock, paper, scissors
    
    var emoji: String{
        switch self {
        case .rock:
            return "👊"
        case .paper:
            return "✋"
        case .scissors:
            return "✌️"
        }
    }
    
    func turn(_ opponent: Sign)->GameState{
        switch self {
        case .rock:
            switch opponent {
            case .rock:
                return GameState.draw
            case .paper:
                return GameState.lose
            case .scissors:
                return GameState.win
            }
        case .paper:
            switch opponent {
            case .rock:
                return GameState.win
            case .paper:
                return GameState.draw
            case .scissors:
                return GameState.lose
            }
        case .scissors:
            switch opponent {
            case .rock:
                return GameState.lose
            case .paper:
                return GameState.win
            case .scissors:
                return GameState.draw
            }
        }
    }
}
