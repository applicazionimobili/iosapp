//
//  ViewController.swift
//  RPS
//
//  Created by Nicolas Palermo on 24/03/2020.
//  Copyright © 2020 Nicolas Palermo. All rights reserved.
//

import UIKit
import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var computerLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var rockButton: UIButton!
    @IBOutlet weak var paperButton: UIButton!
    @IBOutlet weak var scissorsButton: UIButton!
    @IBOutlet weak var playAgainButton: UIButton!
    
    var currentGameState: GameState = GameState.start
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
        resetGame()
    }
        
/** resetGame resetta le componenti grafiche per incominciare una nuova partita.
    Le emoji vengono rese visibili e ablitate al click dell'utente*/
    func resetGame(){
        computerLabel.text = "🤖"
        statusLabel.text = "Rock, Paper, Scissors?"
        rockButton.isHidden = false
        rockButton.isEnabled = true
        paperButton.isHidden = false
        paperButton.isEnabled = true
        scissorsButton.isHidden = false
        scissorsButton.isEnabled = true
        playAgainButton.isHidden = true
        self.view.backgroundColor = UIColor.white

    }
  
    /**Nella funziona playGame, dopo che l'utente fa la propria scelta,
     vengono disabilitate e nascotate le altre emoji. Viene assegnato all'avversario virtuale
     un valore che rappresenta la sua emoji e viene mostrato il risultato della
     partita attaverso "currentStateGame" che richiama l'opportuno metodo
     nella classe Sign che calcola vittoria/sconfitta/pareggio.
     A seconda del valore di current game viene mostato il risultato della
     partita.*/
    func playGame(_ player: Sign){
        rockButton.isEnabled = false
        paperButton.isEnabled = false
        scissorsButton.isEnabled = false
        
        let opponent = randomSign()
        computerLabel.text = opponent.emoji
        
        currentGameState = player.turn(opponent)
        
        switch currentGameState {
        case .draw:
            statusLabel.text = "It's a draw."
            self.view.backgroundColor = UIColor.gray
        case .lose:
            statusLabel.text = "You have lose."
            self.view.backgroundColor = UIColor.red
        case .win:
            statusLabel.text = "You have win."
            self.view.backgroundColor = UIColor.green
        case .start:
            statusLabel.text = "Rock, Paper, Scissors?"
        
        }
        
        
      switch  player {
        case .rock:
            rockButton.isHidden = false
            paperButton.isHidden = true
            scissorsButton.isHidden = true
        case .paper:
            rockButton.isHidden = true
            paperButton.isHidden = false
            scissorsButton.isHidden = true
        case .scissors:
            rockButton.isHidden = true
            paperButton.isHidden = true
            scissorsButton.isHidden = false

        }
    
        playAgainButton.isHidden = false
    }
 
   /**Utente clicca sul bottone per ricominciare una
     nuova partita*/
    @IBAction func playAgainSelected(_ sender: Any) {
        resetGame()
    }
    
    /**Utente sceglie emoji "sasso"*/
    @IBAction func rockSelected(_ sender: Any) {
        playGame(Sign.rock)
    }
     /**Utente clicca su emoji "carta"*/
    @IBAction func paperSelected(_ sender: Any) {
        playGame(Sign.paper)
    }
 
    /**Utente clicca su emoji "forbici"*/
    @IBAction func scissorsSelected(_ sender: Any) {
        playGame(Sign.scissors)
    }
    

}
