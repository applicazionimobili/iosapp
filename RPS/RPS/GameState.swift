//
//  GameState.swift
//  RPS
//
//  Created by Nicolas Palermo on 06/03/2020.
//  Copyright © 2020 Nicolas Palermo. All rights reserved.
//

import Foundation
/**Enumerativo che rappresenta le fasi del gioco.*/
enum GameState{
    case start, win, lose, draw
}
